# SeConf

SeConf - Cloud Config Rectifier

Retifies all the misconfiguration in our AWS cloud account according to CIS Benchmarks.

## Dependencies

- Need to install `awscli` in to your machine
- `python3` must be installed.

## How to run

First you need have a `AWS Access KeyId` and `Secret Key`

Now follow these steps

- Type `aws configure` in CLI.
- Now enter the Access Key, secret key and choose you region.
- Now create a virtual environment using this command `python3 -m venv seconf_venv`
- Now run `source seconf_venv\bin\activate`
- Now run `pip install -r requirements.txt`

> Now you are good to go..

## Run the script

> python3 aws_seconf.py [ -r | -h | -v ]

## Usage

```
usage: python3 SeConf [optional arguments]

AWS Cloud Config Rectifier

optional arguments:
  -h, --help       show this help message and exit
  -r, --remediate  To remediate your AWS account according to CIS Benchmark
  -v, --version    Display version of the tool
```