from boto3 import client

aws_client_cloudwatch = client('cloudwatch')
aws_client_logs = client('logs')
aws_client_sns = client('sns')
LOG_GROUP_NAME = ''
SNS_TOPIC_NAME = ''
SNS_SUB_EMAIL = ''
SNS_TOPIC_ARN = ''
ALARM_NAME = 'unauthorized_api_calls_alarm'
METRIC_NAME = 'unauthorized_api_calls_metric'
METRIC_NAMESPACE = 'CISBenchmark'


def log_metric_and_alarm():
    while True:
        try:
            print("3.1 Ensure a log metric filter and alarm exist for unauthorized API calls")
            global LOG_GROUP_NAME
            LOG_GROUP_NAME = input("Enter existing log group name: ")
            aws_client_logs.put_metric_filter(
                logGroupName=LOG_GROUP_NAME,
                filterName=METRIC_NAME,
                filterPattern='{ ($.errorCode = "*UnauthorizedOperation") || ($.errorCode = "AccessDenied*") }',
                metricTransformations=[
                    {
                        'metricName': METRIC_NAME,
                        'metricNamespace': 'CISBenchmark',
                        'metricValue': '1'
                    }
                ]
            )
            global SNS_TOPIC_NAME
            SNS_TOPIC_NAME = input("Enter your SNS topic Name: ")
            sns_topic_arn = aws_client_sns.create_topic(
                Name=SNS_TOPIC_NAME
            )
            global SNS_SUB_EMAIL
            SNS_SUB_EMAIL = input("Enter email address to SNS subricbe: ")
            global SNS_TOPIC_ARN
            SNS_TOPIC_ARN = sns_topic_arn['TopicArn']
            aws_client_sns.subscribe(
                TopicArn=SNS_TOPIC_ARN,
                Protocol='email',
                Endpoint=SNS_SUB_EMAIL
            )
            aws_client_cloudwatch.put_metric_alarm(
                AlarmName='unauthorized_api_calls_alarm',
                MetricName='unauthorized_api_calls_metric',
                Statistic='Sum',
                Period=300,
                Threshold=1,
                ComparisonOperator='GreaterThanOrEqualToThreshold',
                EvaluationPeriods=1,
                Namespace=METRIC_NAMESPACE,
                AlarmActions=[SNS_TOPIC_ARN]
            )
            print("==> Alarm Created Successfully")
            print("==> Completed!!")
            break
        except aws_client_logs.exceptions.ResourceNotFoundException:
            print("The specified log group does not exist.")
            continue


def log_metric_filter_for_no_MFA():
    print('3.2 Ensure a log metric filter and alarm exist for Management Console sign-in without MFA')
    aws_client_logs.put_metric_filter(
        logGroupName=LOG_GROUP_NAME,
        filterName='no_mfa_console_signin_metric',
        filterPattern='{ ($.eventName = "ConsoleLogin") && ($.additionalEventData.MFAUsed != "Yes") }',
        metricTransformations=[
            {
                'metricName': 'no_mfa_console_signin_metric',
                'metricNamespace': 'CISBenchmark',
                'metricValue': '1'
            },
        ]
    )
    aws_client_cloudwatch.put_metric_alarm(
        AlarmName='no_mfa_console_signin_alarm',
        MetricName='no_mfa_console_signin_metric',
        Statistic='Sum',
        Period=300,
        Threshold=1,
        ComparisonOperator='GreaterThanOrEqualToThreshold',
        EvaluationPeriods=1,
        Namespace=METRIC_NAMESPACE,
        AlarmActions=[SNS_TOPIC_ARN]
    )
    print("Log metric filter and alarm for root account enabled successfully")


def log_metric_filter_for_root_login():
    print('3.3 Ensure a log metric filter and alarm exist for usage of "root" account')
    aws_client_logs.put_metric_filter(
        logGroupName=LOG_GROUP_NAME,
        filterName='root_usage_metric',
        filterPattern='{ $.userIdentity.type = "Root" && $.userIdentity.invokedBy NOT EXISTS && $.eventType != "AwsServiceEvent" }',
        metricTransformations=[
            {
                'metricName': 'root_usage_metric',
                'metricNamespace': 'CISBenchmark',
                'metricValue': '1'
            },
        ]
    )
    aws_client_cloudwatch.put_metric_alarm(
        AlarmName='root_usage_alarm',
        MetricName='root_usage_metric',
        Statistic='Sum',
        Period=300,
        Threshold=1,
        ComparisonOperator='GreaterThanOrEqualToThreshold',
        EvaluationPeriods=1,
        Namespace=METRIC_NAMESPACE,
        AlarmActions=[SNS_TOPIC_ARN]
    )
    print("==> Completed!!")


def log_metric_filter_for_iam():
    print("3.4 Ensure a log metric filter and alarm exist for IAM policy changes")
    aws_client_logs.put_metric_filter(
        logGroupName=LOG_GROUP_NAME,
        filterName='iam_changes_metric',
        filterPattern='{($.eventName=DeleteGroupPolicy)||($.eventName=DeleteRolePolicy)||($.eventName=DeleteUserPolicy)||($.eventName=PutGroupPolicy)||($.eventName=PutRolePolicy)||($.eventName=PutUserPolicy)||($.eventName=CreatePolicy)||($.eventName=DeletePolicy)||($.eventName=CreatePolicyVersion)||($.eventName=DeletePolicyVersion)||($.eventName=AttachRolePolicy)||($.eventName=DetachRolePolicy)||($.eventName=AttachUserPolicy)||($.eventName=DetachUserPolicy)||($.eventName=AttachGroupPolicy)||($.eventName=DetachGroupPolicy)}',
        metricTransformations=[
            {
                'metricName': 'iam_changes_metric',
                'metricNamespace': 'CISBenchmark',
                'metricValue': '1'
            },
        ]
    )
    aws_client_cloudwatch.put_metric_alarm(
        AlarmName='iam_changes_alarm',
        MetricName='iam_changes_metric',
        Statistic='Sum',
        Period=300,
        Threshold=1,
        ComparisonOperator='GreaterThanOrEqualToThreshold',
        EvaluationPeriods=1,
        Namespace=METRIC_NAMESPACE,
        AlarmActions=[SNS_TOPIC_ARN]
    )
    print("==> Completed!!")


def log_metric_filter_for_cloudtrail():
    print("3.5 Ensure a log metric filter and alarm exist for CloudTrail configuration changes")
    aws_client_logs.put_metric_filter(
        logGroupName=LOG_GROUP_NAME,
        filterName='cloudtrail_cfg_changes_metric',
        filterPattern='{ ($.eventName = CreateTrail) || ($.eventName = UpdateTrail) || ($.eventName = DeleteTrail) || ($.eventName = StartLogging) || ($.eventName = StopLogging) }',
        metricTransformations=[
            {
                'metricName': 'cloudtrail_cfg_changes_metric',
                'metricNamespace': 'CISBenchmark',
                'metricValue': '1'
            },
        ]
    )
    aws_client_cloudwatch.put_metric_alarm(
        AlarmName='cloudtrail_cfg_changes_alarm',
        MetricName='cloudtrail_cfg_changes_metric',
        Statistic='Sum',
        Period=300,
        Threshold=1,
        ComparisonOperator='GreaterThanOrEqualToThreshold',
        EvaluationPeriods=1,
        Namespace=METRIC_NAMESPACE,
        AlarmActions=[SNS_TOPIC_ARN]
    )
    print("==> Completed!!")


def log_metric_filter_for_auth_failure():
    print("3.6 Ensure a log metric filter and alarm exist for AWS Management Console authentication failures")
    aws_client_logs.put_metric_filter(
        logGroupName=LOG_GROUP_NAME,
        filterName='console_signin_failure_metric',
        filterPattern='{ ($.eventName = ConsoleLogin) && ($.errorMessage = "Failed authentication") }',
        metricTransformations=[
            {
                'metricName': 'console_signin_failure_metric',
                'metricNamespace': 'CISBenchmark',
                'metricValue': '1'
            },
        ]
    )
    aws_client_cloudwatch.put_metric_alarm(
        AlarmName='console_signin_failure_alarm',
        MetricName='console_signin_failure_metric',
        Statistic='Sum',
        Period=300,
        Threshold=1,
        ComparisonOperator='GreaterThanOrEqualToThreshold',
        EvaluationPeriods=1,
        Namespace=METRIC_NAMESPACE,
        AlarmActions=[SNS_TOPIC_ARN]
    )
    print("==> Completed!!")


def log_metric_filter_for_kms_key_deletion():
    print("3.7 Ensure a log metric filter and alarm exist for disabling or scheduled deletion of customer created CMKs")
    aws_client_logs.put_metric_filter(
        logGroupName=LOG_GROUP_NAME,
        filterName='disable_or_delete_cmk_changes_metric',
        filterPattern='{($.eventSource = kms.amazonaws.com) && (($.eventName=DisableKey)||($.eventName=ScheduleKeyDeletion)) }',
        metricTransformations=[
            {
                'metricName': 'disable_or_delete_cmk_changes_metric',
                'metricNamespace': 'CISBenchmark',
                'metricValue': '1'
            },
        ]
    )
    aws_client_cloudwatch.put_metric_alarm(
        AlarmName='disable_or_delete_cmk_changes_alarm',
        MetricName='disable_or_delete_cmk_changes_metric',
        Statistic='Sum',
        Period=300,
        Threshold=1,
        ComparisonOperator='GreaterThanOrEqualToThreshold',
        EvaluationPeriods=1,
        Namespace=METRIC_NAMESPACE,
        AlarmActions=[SNS_TOPIC_ARN]
    )
    print("==> Completed!!")


def log_metric_filter_for_s3_policy_changes():
    print("3.8 Ensure a log metric filter and alarm exist for S3 bucket policy changes")
    aws_client_logs.put_metric_filter(
        logGroupName=LOG_GROUP_NAME,
        filterName='s3_bucket_policy_changes_metric',
        filterPattern='{ ($.eventSource = s3.amazonaws.com) && (($.eventName = PutBucketAcl) || ($.eventName = PutBucketPolicy) || ($.eventName = PutBucketCors) || ($.eventName = PutBucketLifecycle) || ($.eventName = PutBucketReplication) || ($.eventName = DeleteBucketPolicy) || ($.eventName = DeleteBucketCors) || ($.eventName = DeleteBucketLifecycle) || ($.eventName = DeleteBucketReplication)) }',
        metricTransformations=[
            {
                'metricName': 's3_bucket_policy_changes_metric',
                'metricNamespace': 'CISBenchmark',
                'metricValue': '1'
            },
        ]
    )
    aws_client_cloudwatch.put_metric_alarm(
        AlarmName='s3_bucket_policy_changes_alarm',
        MetricName='s3_bucket_policy_changes_metric',
        Statistic='Sum',
        Period=300,
        Threshold=1,
        ComparisonOperator='GreaterThanOrEqualToThreshold',
        EvaluationPeriods=1,
        Namespace=METRIC_NAMESPACE,
        AlarmActions=[SNS_TOPIC_ARN]
    )
    print("==> Completed!!")


def log_metric_filter_for_aws_config_changes():
    print("3.9 Ensure a log metric filter and alarm exist for AWS Config configuration changes")
    aws_client_logs.put_metric_filter(
        logGroupName=LOG_GROUP_NAME,
        filterName='aws_config_changes_metric',
        filterPattern='{ ($.eventSource = config.amazonaws.com) && (($.eventName=StopConfigurationRecorder)||($.eventName=DeleteDeliveryChannel) || ($.eventName=PutDeliveryChannel)||($.eventName=PutConfigurationRecorder)) }',
        metricTransformations=[
            {
                'metricName': 'aws_config_changes_metric',
                'metricNamespace': 'CISBenchmark',
                'metricValue': '1'
            },
        ]
    )
    aws_client_cloudwatch.put_metric_alarm(
        AlarmName='aws_config_changes_alarm',
        MetricName='aws_config_changes_metric',
        Statistic='Sum',
        Period=300,
        Threshold=1,
        ComparisonOperator='GreaterThanOrEqualToThreshold',
        EvaluationPeriods=1,
        Namespace=METRIC_NAMESPACE,
        AlarmActions=[SNS_TOPIC_ARN]
    )
    print("==> Completed!!")


def log_metric_filter_for_security_grp_changes():
    print("3.10 Ensure a log metric filter and alarm exist for security group changes")
    aws_client_logs.put_metric_filter(
        logGroupName=LOG_GROUP_NAME,
        filterName='security_group_changes_metric',
        filterPattern='{ ($.eventName = AuthorizeSecurityGroupIngress) || ($.eventName = AuthorizeSecurityGroupEgress) || ($.eventName = RevokeSecurityGroupIngress) || ($.eventName = RevokeSecurityGroupEgress) ||($.eventName = CreateSecurityGroup) || ($.eventName = DeleteSecurityGroup) }',
        metricTransformations=[
            {
                'metricName': 'security_group_changes_metric',
                'metricNamespace': 'CISBenchmark',
                'metricValue': '1'
            },
        ]
    )
    aws_client_cloudwatch.put_metric_alarm(
        AlarmName='security_group_changes_alarm',
        MetricName='security_group_changes_metric',
        Statistic='Sum',
        Period=300,
        Threshold=1,
        ComparisonOperator='GreaterThanOrEqualToThreshold',
        EvaluationPeriods=1,
        Namespace=METRIC_NAMESPACE,
        AlarmActions=[SNS_TOPIC_ARN]
    )
    print("==> Completed!!")


def log_metric_filter_for_nacl_changes():
    print("3.11 Ensure a log metric filter and alarm exist for changes to Network Access Control Lists (NACL)")
    aws_client_logs.put_metric_filter(
        logGroupName=LOG_GROUP_NAME,
        filterName='nacl_changes_metric',
        filterPattern='{ ($.eventName = CreateNetworkAcl) || ($.eventName = CreateNetworkAclEntry) || ($.eventName = DeleteNetworkAcl) || ($.eventName = DeleteNetworkAclEntry) || ($.eventName = ReplaceNetworkAclEntry) || ($.eventName = ReplaceNetworkAclAssociation) }',
        metricTransformations=[
            {
                'metricName': 'nacl_changes_metric',
                'metricNamespace': 'CISBenchmark',
                'metricValue': '1'
            },
        ]
    )
    aws_client_cloudwatch.put_metric_alarm(
        AlarmName='nacl_changes_alarm',
        MetricName='nacl_changes_metric',
        Statistic='Sum',
        Period=300,
        Threshold=1,
        ComparisonOperator='GreaterThanOrEqualToThreshold',
        EvaluationPeriods=1,
        Namespace=METRIC_NAMESPACE,
        AlarmActions=[SNS_TOPIC_ARN]
    )
    print("==> Completed!!")


def log_metric_filter_for_netgate_changes():
    print("3.12 Ensure a log metric filter and alarm exist for changes to network gateways")
    aws_client_logs.put_metric_filter(
        logGroupName=LOG_GROUP_NAME,
        filterName='network_gw_changes_metric',
        filterPattern='{ ($.eventName = CreateCustomerGateway) || ($.eventName = DeleteCustomerGateway) || ($.eventName = AttachInternetGateway) || ($.eventName = CreateInternetGateway) || ($.eventName = DeleteInternetGateway) || ($.eventName = DetachInternetGateway) }',
        metricTransformations=[
            {
                'metricName': 'network_gw_changes_metric',
                'metricNamespace': 'CISBenchmark',
                'metricValue': '1'
            },
        ]
    )
    aws_client_cloudwatch.put_metric_alarm(
        AlarmName='network_gw_changes_alarm',
        MetricName='network_gw_changes_metric',
        Statistic='Sum',
        Period=300,
        Threshold=1,
        ComparisonOperator='GreaterThanOrEqualToThreshold',
        EvaluationPeriods=1,
        Namespace=METRIC_NAMESPACE,
        AlarmActions=[SNS_TOPIC_ARN]
    )
    print("==> Completed!!")


def log_metric_filter_for_route_table_changes():
    print("3.13 Ensure a log metric filter and alarm exist for route table changes")
    aws_client_logs.put_metric_filter(
        logGroupName=LOG_GROUP_NAME,
        filterName='route_table_changes_metric',
        filterPattern='{ ($.eventName = CreateRoute) || ($.eventName = CreateRouteTable) || ($.eventName = ReplaceRoute) || ($.eventName = ReplaceRouteTableAssociation) || ($.eventName = DeleteRouteTable) || ($.eventName = DeleteRoute) || ($.eventName = DisassociateRouteTable) }',
        metricTransformations=[
            {
                'metricName': 'route_table_changes_metric',
                'metricNamespace': 'CISBenchmark',
                'metricValue': '1'
            },
        ]
    )
    aws_client_cloudwatch.put_metric_alarm(
        AlarmName='route_table_changes_alarm',
        MetricName='route_table_changes_metric',
        Statistic='Sum',
        Period=300,
        Threshold=1,
        ComparisonOperator='GreaterThanOrEqualToThreshold',
        EvaluationPeriods=1,
        Namespace=METRIC_NAMESPACE,
        AlarmActions=[SNS_TOPIC_ARN]
    )
    print("==> Completed!!")


def log_metric_filter_for_vpc_changes():
    print("3.14 Ensure a log metric filter and alarm exist for VPC changes")
    aws_client_logs.put_metric_filter(
        logGroupName=LOG_GROUP_NAME,
        filterName='vpc_changes_metric',
        filterPattern='{ ($.eventName = CreateVpc) || ($.eventName = DeleteVpc) || \
            ($.eventName = ModifyVpcAttribute) || ($.eventName = AcceptVpcPeeringConnection) || \
            ($.eventName = CreateVpcPeeringConnection) || ($.eventName = DeleteVpcPeeringConnection) || ($.eventName = RejectVpcPeeringConnection) || ($.eventName = AttachClassicLinkVpc) || ($.eventName = DetachClassicLinkVpc) || \
            ($.eventName = DisableVpcClassicLink) || ($.eventName = EnableVpcClassicLink) }',
        metricTransformations=[
            {
                'metricName': 'vpc_changes_metric',
                'metricNamespace': 'CISBenchmark',
                'metricValue': '1'
            },
        ]
    )
    aws_client_cloudwatch.put_metric_alarm(
        AlarmName='vpc_changes_alarm',
        MetricName='vpc_changes_metric',
        Statistic='Sum',
        Period=300,
        Threshold=1,
        ComparisonOperator='GreaterThanOrEqualToThreshold',
        EvaluationPeriods=1,
        Namespace=METRIC_NAMESPACE,
        AlarmActions=[SNS_TOPIC_ARN]
    )
    print("==> Completed!!")


def main():
    log_metric_and_alarm()
    log_metric_filter_for_no_MFA()
    log_metric_filter_for_root_login()
    log_metric_filter_for_iam()
    log_metric_filter_for_cloudtrail()
    log_metric_filter_for_auth_failure()
    log_metric_filter_for_kms_key_deletion()
    log_metric_filter_for_s3_policy_changes()
    log_metric_filter_for_aws_config_changes()
    log_metric_filter_for_security_grp_changes()
    log_metric_filter_for_nacl_changes()
    log_metric_filter_for_netgate_changes()
    log_metric_filter_for_route_table_changes()
    log_metric_filter_for_vpc_changes()
