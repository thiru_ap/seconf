from json import dumps

from boto3 import client
aws_client = client('cloudtrail')
aws_client_config = client('config')
aws_client_kms = client('kms')


def cloudtrail_enabled_all_regions():
    print("2.1 Ensure CloudTrail is enabled in all regions (Scored)")
    print("==> Checking whether cloud trail is enabled in all regions")
    try:
        response = aws_client.describe_trails()
        trial_list = response['trailList']
        if not trial_list:
            trial_name = input("Enter the trial name: ")
            bucket_name = input(
                "Enter an exisiting bucket name: ")
            ct_trail = aws_client.create_trail(
                Name=trial_name,
                S3BucketName=bucket_name,
                IsMultiRegionTrail=True
            )
            if ct_trail['Name'] == trial_name:
                aws_client.update_trail(
                    Name=ct_trail['Name'],
                    IsMultiRegionTrail=True
                )
                aws_client.start_logging(
                    Name=trial_name
                )
        for each_trial in trial_list:
            trial_arn = each_trial['Name']
            trial_name = each_trial['Name']
            res = aws_client.get_trail_status(
                Name=trial_arn)
            event_selectors = aws_client.get_event_selectors(
                TrailName=trial_name
            )
            event_selector = event_selectors['EventSelectors']
            for event in event_selector:
                if each_trial['IsMultiRegionTrail'] is True and res['IsLogging'] is True and event['IncludeManagementEvents'] is True and event['ReadWriteType'] == 'All':
                    print("==> The benchmark is satisfied")
                    return
                else:
                    try:
                        aws_client.start_logging(
                            Name=trial_name
                        )
                        aws_client.update_trail(
                            Name=trial_name,
                            IsMultiRegionTrail=True,
                            IncludeGlobalServiceEvents=True
                        )
                        break
                    except aws_client.exceptions.TrailNotFoundException:
                        print("!!> Invalid Trail")

    except aws_client.exceptions.TrailNotFoundException:
        while True:
            try:
                trial_name = input("Enter the trial name: ")
                bucket_name = input("Enter an exisiting bucket name: ")
                ct_trail = aws_client.create_trail(
                    Name=trial_name,
                    S3BucketName=bucket_name,
                    IsMultiRegionTrail=True
                )
                if ct_trail['Name'] == bucket_name:
                    aws_client.update_trail(
                        Name=ct_trail['Name'],
                        IsMultiRegionTrail=True
                    )
                    break
            except aws_client.exceptions.S3BucketDoesNotExistException:
                print("The S3 bucket does not exist")
                continue
            except aws_client.exceptions.TrailAlreadyExistsException:
                print("Trial Name already Exists")
                continue
            except aws_client.exceptions.InvalidTrailNameException:
                print("Invalid Trial Name..")


def cloudtrail_logfile_validation():
    print("2.2 Ensure CloudTrail log file validation is enabled")
    print("==> Checking whether Log File Validation are enabled")
    try:
        cloud_trails = aws_client.describe_trails()
        all_trials = cloud_trails['trailList']
        for each_trial in all_trials:
            if each_trial['LogFileValidationEnabled'] is False:
                aws_client.update_trail(
                    Name=each_trial['Name'],
                    EnableLogFileValidation=True
                )
        print("==> Log file validation is updated to all CloudTrail trials")
    except Exception:
        print(">>! AN error occured")


def cloudtrail_integrated_cloudwatch_logs():
    print("2.4 Ensure CloudTrail trails are integrated with CloudWatch Logs")
    try:
        cloudtrail = aws_client.describe_trails()
        cloudtrail_list = cloudtrail['trailList']
        for each_trial in cloudtrail_list:
            trial_status = aws_client.get_trail_status(
                Name=each_trial['Name']
            )
            if not each_trial['CloudWatchLogsLogGroupArn']:
                raise KeyError
    except KeyError:
        while True:
            try:
                trail_name = input("Enter trail name: ")
                cloud_watch_log_group_name = input(
                    "Enter existing Cloudwatch group ARN: ")
                cloud_watc_role_arn = input("Enter CloudWatch role ARN: ")
                aws_client.update_trail(
                    Name=trail_name,
                    CloudWatchLogsLogGroupArn=cloud_watch_log_group_name,
                    CloudWatchLogsRoleArn=cloud_watc_role_arn
                )
                print("CloudTrail are integrated with CloudWatch Logs Successfully")
                break
            except aws_client.exceptions.TrailNotFoundException:
                print("The trail name is not found")
                continue


def cloudtrail_log_encrypted():
    print("2.7 Ensure CloudTrail logs are encrypted at rest using KMS CMKs")
    try:
        cloudtrail_encrypted = aws_client.describe_trails()
        trail_list = cloudtrail_encrypted['trailList']
        for trail in trail_list:
            dict_keys = list(trail.keys())
            if 'KmsKeyId' not in dict_keys:
                trail_name = trail['Name']
                kms_policy_dict = {
                    "Version": "2012-10-17",
                    "Id": "key-default-5",
                    "Statement": [
                        {
                            "Sid": "Enable IAM User Permissions",
                            "Effect": "Allow",
                            "Principal": {
                                "AWS": "arn:aws:iam::614171866298:root"
                            },
                            "Action": "kms:*",
                            "Resource": "*"
                        },
                        {
                            "Sid": "Allow CloudTrail to encrypt logs",
                            "Effect": "Allow",
                            "Principal": {
                                "Service": "cloudtrail.amazonaws.com"
                            },
                            "Action": "kms:GenerateDataKey*",
                            "Resource": "*",
                            "Condition": {
                                "StringLike": {
                                    "kms:EncryptionContext:aws:cloudtrail:arn": "arn:aws:cloudtrail:*:614171866298:trail/*"
                                }
                            }
                        },
                        {
                            "Sid": "Enable CloudTrail log decrypt permissions",
                            "Effect": "Allow",
                            "Principal": {
                                "AWS": "arn:aws:iam::614171866298:user/tamiZh"
                            },
                            "Action": "kms:Decrypt",
                            "Resource": "*",
                            "Condition": {
                                "Null": {
                                    "kms:EncryptionContext:aws:cloudtrail:arn": "false"
                                }
                            }
                        },
                        {
                            "Sid": "Allow CloudTrail access",
                            "Effect": "Allow",
                            "Principal": {
                                "Service": "cloudtrail.amazonaws.com"
                            },
                            "Action": "kms:DescribeKey",
                            "Resource": "*"
                        }
                    ]
                }
                kms_policy_string = dumps(kms_policy_dict)
                while True:
                    try:
                        kms_key_id = input("Enter the KMS Key Id: ")
                        aws_client.update_trail(
                            Name=trail_name,
                            KmsKeyId=kms_key_id
                        )
                        aws_client_kms.put_key_policy(
                            KeyId=kms_key_id,
                            PolicyName='default',
                            Policy=kms_policy_string
                        )
                        print("==> CloudTrail log are encrypted sucessfully.")
                        break
                    except aws_client.exceptions.InsufficientEncryptionPolicyException:
                        print("!!> Cannot add key due to insufficient Encryption")
                    except aws_client.exceptions.InvalidKmsKeyIdException:
                        print("!!> Key not found enter proper key-id.")
                        continue
    except Exception as e:
        print(e)


def kms_cutomer_keyrotation_enabled():
    print("2.8 Ensure rotation for customer created CMKs is enabled")
    kms_keys = aws_client_kms.list_keys()
    all_keys = kms_keys['Keys']
    if not all_keys:
        print("==> No keys found in KMS.")
        return
    for each_key in all_keys:
        key_status = aws_client_kms.get_key_rotation_status(
            KeyId=each_key['KeyId']
        )
        if key_status['KeyRotationEnabled'] is False:
            try:
                aws_client_kms.enable_key_rotation(
                    KeyId=each_key['KeyId']
                )
            except aws_client_kms.exceptions.KMSInvalidStateException:
                pass
            except aws_client_kms.exceptions.AccessDeniedException:
                print("!!> You don't have enough permission to rotate keys.")
    print("==> Key Rotation enabled for all keys sucessfully.")


def main():
    cloudtrail_enabled_all_regions()
    cloudtrail_logfile_validation()
    cloudtrail_integrated_cloudwatch_logs()
    cloudtrail_log_encrypted()
    kms_cutomer_keyrotation_enabled()
