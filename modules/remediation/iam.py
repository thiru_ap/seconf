from boto3 import client, resource
from datetime import datetime, date, timedelta

aws_client = client('iam')
aws_iam = resource('iam')


def iam_update_password_policy():
    # CIS 1.5, 1.6, 1.7, 1.8, 1.9, 1.10, 1.11
    """ IAM update password policy

    Equivalent to this command : aws iam update-account-password-policy  
    """
    print("Running CIS 1.5, 1.6, 1.7, 1.8, 1.9, 1.10, 1.11")
    print("==> Updating IAM Password policy")

    try:
        response = aws_client.update_account_password_policy(
            MinimumPasswordLength=10,
            RequireSymbols=True,
            RequireNumbers=True,
            RequireUppercaseCharacters=True,
            RequireLowercaseCharacters=True,
            MaxPasswordAge=90,
            PasswordReusePrevention=24
        )
        if response["ResponseMetadata"]["HTTPStatusCode"] == 200:
            print("==> Update IAM Password Policy successfully")

    except Exception as e:
        print("!!> A problem occured!!", e)


def iam_support_role():
    # CIS 1.20
    print("1.20 Ensure a support role has been created to manage incidents with AWS Support")
    print("==> Checking policies..")
    response = aws_client.list_entities_for_policy(
        PolicyArn='arn:aws:iam::aws:policy/AWSSupportAccess'
    )
    if not response['PolicyGroups'] and not response['PolicyUsers'] and not response['PolicyRoles']:
        role_name = 'aws_support_iam_role'
        while True:
            user_name = input(
                "Enter existing support username to manage incidents: ")
            try:
                user = aws_iam.User(user_name)
                arn = str(user.arn)
                # arn = input("Enter user ARN: ")
                trust_policy = '{"Version": "2012-10-17","Statement": [{"Effect": "Allow","Principal": {"AWS" : "' + \
                    arn+'"},"Action": "sts:AssumeRole"}]}'
                aws_client.create_role(
                    RoleName=role_name,
                    AssumeRolePolicyDocument=trust_policy
                )
                role_resp = aws_client.attach_role_policy(
                    RoleName=role_name,
                    PolicyArn='arn:aws:iam::aws:policy/AWSSupportAccess'
                )

                if role_resp["ResponseMetadata"]["HTTPStatusCode"] == 200:
                    print("==> Updated Support role successfully")
                    break
            except aws_client.exceptions.NoSuchEntityException as _:
                print("!!> No such username exists.")
                continue


def iam_no_administrative_privilege():
    # 1.22 Ensure IAM policies that allow full "*:*" administrative privileges are not created
    print("1.22 Ensure IAM policies that allow full '*: *' administrative privileges are not created")
    print("==> Checking whether administrative privileges are set.")
    try:
        user_managed_policies = aws_client.list_policies(
            Scope='Local'
        )
        if not user_managed_policies["Policies"]:
            print("==> No User managed policy found.")
            return
        policies = user_managed_policies['Policies']
        for policy in policies:
            policy_arn = policy['Arn']
            policy_version_id = policy['DefaultVersionId']
            policy_statement = aws_iam.PolicyVersion(
                policy_arn,
                policy_version_id
            )
            try:
                statement = policy_statement.document['Statement']
                for each_statement in statement:
                    if each_statement['Effect'] == 'Allow' and each_statement['Action'] == '*' and each_statement['Resource'] == '*':
                        res = aws_client.list_entities_for_policy(
                            PolicyArn=policy_arn
                        )
                        if res['PolicyGroups']:
                            groups = res['PolicyGroups']
                            for group in groups:
                                aws_client.detach_group_policy(
                                    GroupName=group['GroupName'],
                                    PolicyArn=policy_arn
                                )
                        if res['PolicyRoles']:
                            users = res['PolicyRoles']
                            for user in users:
                                aws_client.detach_role_policy(
                                    RoleName=user['RoleName'],
                                    PolicyArn=policy_arn
                                )
                        if res['PolicyUsers']:
                            roles = res['PolicyUsers']
                            for role in roles:
                                aws_client.detach_user_policy(
                                    UserName=role['UserName'],
                                    PolicyArn=policy_arn
                                )
                        print(
                            "==> Removed administrative privileges for users, groups and roles")
            except Exception as e:
                print(e)
    except Exception as _:
        pass


def main():
    iam_update_password_policy()
    iam_support_role()
    iam_no_administrative_privilege()
