from argparse import ArgumentParser, RawTextHelpFormatter

""" To parse the arguments given by the user
"""

argsparser = ArgumentParser(prog='SeConf',
                            description='AWS Cloud Config Rectifier',
                            usage='python3 %(prog)s [optional arguments]',
                            formatter_class=RawTextHelpFormatter)

args_group = argsparser.add_mutually_exclusive_group(required=True)

args_group.add_argument('-r', '--remediate', action='store_true',
                        help='To remediate your AWS account according to CIS Benchmark')

args_group.add_argument('-v', '--version', action='store_true',
                        help='Display version of the tool')
