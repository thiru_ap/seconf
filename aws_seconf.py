from modules.argumentparser import argsparser
from modules.remediation import iam, logging, monitoring

def main():
    arg = argsparser.parse_args()

    if arg.remediate:
        iam.main()
        logging.main()
        monitoring.main()
    elif arg.version:
        print("AWS_SECONF version 0.1")


if __name__ == "__main__":
    main()
