from setuptools import setup, find_packages


with open('README.md') as f:
    readme = f.read()

with open('LICENSE') as f:
    license = f.read()

setup(
    name='aws_seconf',
    version='0.1.0',
    description='AWS CIS Benchmark',
    long_description=readme,
    author='Jayasri G S',
    author_email='jsricanada444@gmail.com',
    url='',
    license=license,
    packages=find_packages(exclude=('tests', 'docs'))
)
